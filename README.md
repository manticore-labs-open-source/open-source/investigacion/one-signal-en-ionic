# One Signal en Ionic
One Signal es una plataforma que permite el envío de Notificaciones Push a través de diferentes plataformas que estén debidamente registradas a nuestra aplicación. Una vez el dispositivo ha sido registrado adecuadamente a nuestra App, ha quedado listo para el envío de push notifications desde nuestro Dashboard. Sin embargo, en varias ocasiones, necesitaremos enviar mensajes específicamente a uno o varios usuarios

Para poder usarla, se debe registra.
![imagen](imagenes/oneSignal.png)

## Añadir Aplicación 
* Para crear una nueva aplicación en One Signal, damos clik en **Add App**

![imagen](imagenes/add-app.png)

* Luego debemos darle un nombre a nuestra aplicación.

![imagen](imagenes/add-name.png)

* Una vez dado el nombre de nuestra app, debemos seleccionar la plataforma en la cual se va a configurar, para este caso usaremos la de Android.

![imagen](imagenes/add-android.png)

## Añadir Aplicación en Firebase

* Para el uso de One Signal, se debe tener una key de [**FIREBASE Server**](https://firebase.google.com), para ello ingresamos a [**Firebase**](https://firebase.google.com), el cual trabajo con Google, se debe tener una cuenta de Google para usar [**Firebase**](https://firebase.google.com), [**Firebase**](https://firebase.google.com) es una plataforma para el desarrollo de aplicaciones web y aplicaciones móviles desarrollada, también, es un conjunto de herramientas orientadas a la creación de aplicaciones de alta calidad, al crecimiento de los usuarios y a ganar más dinero.

![imagen](imagenes/firebase.png)

* Nos dirigimos a **Ir a consola**

![imagen](imagenes/firebase-consola.png)

* Agregamos un nuevo proyecto

![imagen](imagenes/firebase-add.png)

* Debemos añadir un nombre y aceptar los términos y condiciones

![imagen](imagenes/firebase-add-name.png)

* Una vez creada nos redirige a la pantalla de inicio del proyecto creado en Firebase 

![imagen](imagenes/firebase-new-add.png)

* Debemos seleccionar configuraciones, y escoger la opción de **Cloud Messaging**, donde encontraremos nuestra **Firebase Server Key** y **Firebase Sender ID**

![imagen](imagenes/firebase-config.png)

* Las keys obtenidas debemos colocarla en la configuración de One Signal

![imagen](imagenes/firebase-key.png)

## Configuración de One Signal en Ionic

* Seleccionamos el SDK en el cual vamos a trabajar, para este caso usaremos en Ionic 

![imagen](imagenes/one-signal-ionic.png)

* Para poder usar en Ionic debemos obtener **ONESIGNAL APP ID** y  **REST API KEY**, que se encuentra dentro de la app que creamos en One Signal, se debe colocar el siguiente bloque de código dentro del archivo **app.component.ts**, dentro del constructor.
![imagen](imagenes/one-signal-api.png)

```javascript
window["plugins"].OneSignal
        .startInit("TU_ONESIGNAL_APP_ID", "TU_API_KEY")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();
```

![imagen](imagenes/one-signal-init.png)

* Para obtener la información de la notificación añadimos el siguiente bloque de código dentro del mismo archivo.

```javascript
var notificationOpenedCallback = function (jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));        
      };
```
![imagen](imagenes/one-signal-data.png)

## Envió de notificaciones desde One Signal

* Par el envió de una notificación nos dirigimos al Dashboard de One Signal, seleccionamos Messeges -> new Push, agregamos el título y el contenido de la notificación Push, se puede agregar más opciones, dependiendo de lo que el usuario necesite. Y finalmente se da en confirmar y en **send** y la notificación será enviada a todos los dispositivos subscritos 
 
![imagen](imagenes/one-signal-message.png)

![imagen](imagenes/one-signal-title.png)

![imagen](imagenes/one-signal-options.png)

![imagen](imagenes/one-signal-options2.png)

![imagen](imagenes/one-signal-options3.png)


<a href="https://twitter.com/kevi_n_24" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @kevi_n_24 </a><br>